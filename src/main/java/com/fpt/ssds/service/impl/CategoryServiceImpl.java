package com.fpt.ssds.service.impl;

import com.fpt.ssds.common.exception.SSDSBusinessException;
import com.fpt.ssds.constant.ErrorConstants;
import com.fpt.ssds.domain.Category;
import com.fpt.ssds.repository.CategoryRepository;
import com.fpt.ssds.service.CategoryService;
import com.fpt.ssds.service.dto.CategoryDto;
import com.fpt.ssds.service.mapper.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public void createUpdate(CategoryDto categoryDto) {
        if(Objects.nonNull(categoryDto.getId())){
            updateCategory(categoryDto);
        }
        createCategory(categoryDto);
    }

    private void createCategory(CategoryDto categoryDto) {
        String code = categoryDto.getCode();
        Optional<Category> categoryOpt = categoryRepository.findByCode(code);
        if(categoryOpt.isPresent()){
            throw new SSDSBusinessException(ErrorConstants.CATEGORY_ALREADY_EXIST, Arrays.asList(code));
        }
        categoryRepository.save(categoryMapper.toEntity(categoryDto));
    }

    private void updateCategory(CategoryDto categoryDto) {
        String code = categoryDto.getCode();
        Optional<Category> categoryOpt = categoryRepository.findByCode(code);
        if(categoryOpt.isPresent()){
            if(!categoryOpt.get().getId().equals(categoryDto.getId())){
                throw new SSDSBusinessException(ErrorConstants.CATEGORY_ALREADY_EXIST, Arrays.asList(code));
            }
        }
        categoryRepository.save(categoryMapper.toEntity(categoryDto));
    }
}
