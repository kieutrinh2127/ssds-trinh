package com.fpt.ssds.service.impl;

import com.fpt.ssds.constant.Constants;
import com.fpt.ssds.repository.CategoryRepository;
import com.fpt.ssds.service.CommonService;
import com.fpt.ssds.service.dto.ResponseDTO;
import com.fpt.ssds.service.dto.SelectionDTO;
import com.fpt.ssds.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommonServiceImpl implements CommonService {
    private final Logger log = LoggerFactory.getLogger(CommonServiceImpl.class);

    private final CategoryRepository categoryRepository;

    @Autowired
    public CommonServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public ResponseDTO getSelectionByType(String type) {
        ResponseDTO responseDTO = ResponseUtils.responseOK(null);
        List<SelectionDTO> selectionDTOS = new ArrayList<>();
        switch (type){
            case Constants.COMMON_TYPE.SERVICE_CATEGORY:
                categoryRepository.findAll().forEach(category -> {
                    selectionDTOS.add(populateSelectionDTO(category.getId(), category.getCode(), category.getName()));
                });
                break;
        }
        responseDTO.setData(selectionDTOS);
        return responseDTO;
    }

    private SelectionDTO populateSelectionDTO(Long id, String code, String name) {
        SelectionDTO selectionDTO = new SelectionDTO();
        selectionDTO.setId(id);
        selectionDTO.setCode(code);
        selectionDTO.setName(name);
        return selectionDTO;
    }
}
