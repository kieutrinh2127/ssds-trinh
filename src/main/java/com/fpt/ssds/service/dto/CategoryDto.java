package com.fpt.ssds.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * A DTO for the {@link com.fpt.ssds.domain.Category} entity
 */
@Data
public class CategoryDto implements Serializable {
    private Long id;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String code;

    private String description;

    private Set<ServiceDto> services;
}
