package com.fpt.ssds.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "restriction")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Restriction extends AbstractAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties(value = "restrictions", allowSetters = true)
    @JoinColumn(name = "role_id")
    private Role role;

    @ManyToOne
    @JsonIgnoreProperties(value = "restrictions", allowSetters = true)
    @JoinColumn(name = "branch_id")
    private Branch branch;

    @ManyToOne
    @JsonIgnoreProperties(value = "restrictions", allowSetters = true)
    @JoinColumn(name = "user_id")
    private User user;
}
