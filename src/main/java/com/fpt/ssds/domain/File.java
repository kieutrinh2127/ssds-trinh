package com.fpt.ssds.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "file")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class File extends AbstractAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "metadata")
    private String metadata;

    @Column(name = "type")
    private String type;

    @Column(name = "storage_source")
    private String storageSource;

    @Column(name = "upload_status")
    private String uploadStatus;

    @Column(name = "fail_reason")
    private String failReason;

    @Column(name = "start_upload_time")
    private Instant startUploadTime;

    @Column(name = "finish_upload_time")
    private Instant finishUploadTime;

    @Column(name = "url")
    private String url;
}
