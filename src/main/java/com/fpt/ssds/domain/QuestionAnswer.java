package com.fpt.ssds.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "question_answer")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class QuestionAnswer extends AbstractAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text_answer")
    private String textAnswer;

    @ManyToOne
    @JsonIgnoreProperties(value = "questionAnswers", allowSetters = true)
    @JoinColumn(name = "question_id")
    private ScaQuestion question;

    @ManyToOne
    @JsonIgnoreProperties(value = "questionAnswers", allowSetters = true)
    @JoinColumn(name = "option_id")
    private Option option;

    @ManyToOne
    @JsonIgnoreProperties(value = "questionAnswers", allowSetters = true)
    @JoinColumn(name = "result_id")
    private ScaResult result;
}
