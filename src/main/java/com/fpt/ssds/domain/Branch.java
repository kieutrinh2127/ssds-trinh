package com.fpt.ssds.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "branch")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Branch extends AbstractAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "detail_address")
    private String detailAddress;

    @Column(name = "detail")
    private String detail;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @OneToMany(mappedBy = "branch")
    @JsonIgnore
    private Set<ConfigData> configDataSet = new HashSet<>();

    @OneToMany(mappedBy = "branch")
    @JsonIgnore
    private Set<EquipmentBranch> equipmentBranches = new HashSet<>();
}
