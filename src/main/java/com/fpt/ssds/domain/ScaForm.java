package com.fpt.ssds.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sca_form")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ScaForm extends AbstractAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "form")
    @JsonIgnore
    private Set<ScaQuestion> questions = new HashSet<>();
}
