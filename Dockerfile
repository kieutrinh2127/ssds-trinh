EXPOSE 8080
COPY target/ $APP_HOME
COPY src/main/resources/config $APP_HOME/config/
COPY src/main/resources/logback*.xml $APP_HOME/config/

COPY target/*.jar $APP_HOME/app.jar

COPY src/main/jib/entrypoint.sh $APP_HOME/bin/entrypoint.sh
RUN chmod a+x $APP_HOME/bin/entrypoint.sh

WORKDIR $APP_HOME

ENTRYPOINT ["bin/entrypoint.sh"]
